package com.zmh.mapper;

import com.zmh.pojo.navigationes;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/*
 *@author zmh
 *@date 2022/5/7-23:30
 */
@Mapper
public interface navigationMapper {
    /**
     * 查询所有
     * @return
     */
    List<navigationes> findNavigation();

    /**
     * 插入导航块
     * @param navigation_text
     * @return
     */
    int InsertNavigation(@Param("navigation_text")String navigation_text);

    /**
     * 根据id删除导航块
     * @param id
     * @return
     */
    int deleteDaoHang(@Param("id")int id);

    /**
     * 更改导航块
     * @param id
     * @param navigation
     * @return
     */
    int updateDaoHang(@Param("id")int id,@Param("navigation")String navigation);
}
