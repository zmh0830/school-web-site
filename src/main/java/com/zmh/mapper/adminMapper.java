package com.zmh.mapper;

import com.zmh.pojo.adminteam;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/*
 *@author zmh
 *@date 2022/5/7-10:19
 */
@Mapper
public interface adminMapper {
    /**
     * 验证账号密码
     * @param adminName
     * @param adminWord
     * @return
     */
    int findId(@Param("adminName")String adminName,@Param("adminWord")String adminWord);

    /**
     * 根据adminName查询个人资料
     * @param adminName
     * @return
     */
    List<adminteam> findMessage(@Param("adminName")String adminName);

    /**
     * 查询所有
     * @return
     */
    List<adminteam> findAdmin();
}
