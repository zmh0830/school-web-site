package com.zmh.mapper;

import com.zmh.pojo.teachMsg;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/*
 *@author zmh
 *@date 2022/5/10-9:29
 */
@Mapper
public interface teachMsgMapper {
    /**
     * 查找所有教学信息
     * @return
     */
    List<teachMsg> findAll();

    /**
     * 根据id查找教学信息
     * @param id
     * @return
     */
    List<teachMsg> findById(@Param("id")int id);

    /**
     * 根据id更改教学信息
     * @param id
     * @param teachMsg
     * @param teachHref
     * @param teachDate
     * @return
     */
    int updateMsgById(@Param("id") int id, @Param("teachMsg")String teachMsg, @Param("teachHref")String teachHref, @Param("teachDate")String teachDate);

    /**
     * 添加教学信息
     * @param teachMsg
     * @param teachHref
     * @param teachDate
     * @return
     */
    int addMsg(@Param("teachMsg")String teachMsg, @Param("teachHref")String teachHref, @Param("teachDate")String teachDate);

    /**
     * 根据id删除教学信息
     * @param id
     * @return
     */
    int deleteMsg(@Param("id") int id);
}
