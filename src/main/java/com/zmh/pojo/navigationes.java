package com.zmh.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 *@author zmh
 *@date 2022/5/8-12:23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class navigationes {
    private int id;
    private String Navigation;
}
