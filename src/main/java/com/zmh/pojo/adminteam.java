package com.zmh.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;

/*
 *@author zmh
 *@date 2022/5/7-10:33
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class adminteam {
    private int adminID;
    private String adminName;
    private String adminWord;
    private String adminEmail;
    private String adminSex;
    private String adminPhone;
    private String adminAddress;
    private String adminLevel;
}
