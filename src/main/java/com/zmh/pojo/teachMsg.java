package com.zmh.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/*
 *@author zmh
 *@date 2022/5/10-9:30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class teachMsg {
    private int id;
    private String teachMsg;
    private String teachHref;
    private String teachDate;
}
