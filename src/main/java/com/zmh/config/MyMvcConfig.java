package com.zmh.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/*
 *@author zmh
 *@date 2022/4/28-0:23
 */
@Configuration
public class MyMvcConfig implements WebMvcConfigurer {
//    视图跳转
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/admin.io").setViewName("login");
        registry.addViewController("/news_list").setViewName("news_list");
        registry.addViewController("/news_add").setViewName("news_add");
        registry.addViewController("/news_edit").setViewName("news_edit");
        registry.addViewController("/welcome").setViewName("welcome");
        //registry.addViewController("/DaoHangList").setViewName("DaoHangList");
        registry.addViewController("/updateDaoHang").setViewName("updateDaoHang");
        registry.addViewController("/addDaoHang").setViewName("addDaoHang");
        registry.addViewController("/shce").setViewName("shce");
    }
}
