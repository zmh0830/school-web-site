package com.zmh.service.impl;

import com.zmh.mapper.adminMapper;
import com.zmh.pojo.adminteam;
import com.zmh.service.adminService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/*
 *@author zmh
 *@date 2022/5/7-10:12
 */
@Service
@Transactional(rollbackFor=Exception.class)
public class adminServiceImpl implements adminService {
    @Resource
    adminMapper adminMapper;

    @Override
    public int findId(String adminName, String adminWord) {
        return adminMapper.findId(adminName, adminWord);
    }

    @Override
    public List<adminteam> findMessage(String adminName) {
        return adminMapper.findMessage(adminName);
    }

    @Override
    public List<adminteam> findAdmin() {
        return adminMapper.findAdmin();
    }


}
