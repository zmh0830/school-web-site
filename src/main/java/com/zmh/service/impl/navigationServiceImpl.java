package com.zmh.service.impl;

import com.zmh.mapper.navigationMapper;
import com.zmh.pojo.navigationes;
import com.zmh.service.navigationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/*
 *@author zmh
 *@date 2022/5/7-23:40
 */
@Service
//如果加在类上，这个类下的所有方法都将会被加上事务管理（rollbackFor=Exception.class只要出现异常就会回滚）
@Transactional(rollbackFor=Exception.class)
public class navigationServiceImpl implements navigationService{
    @Resource
    navigationMapper navigationMapper;

    /**
     * 查询导航栏内容
     * @return
     */
    @Override
    public List<navigationes> findNavigation() {
        return  navigationMapper.findNavigation();
    }

    /**
     * 添加导航栏内容
     * @param navigation_text
     * @return
     */
    @Override
    public String InsertNavigation(String navigation_text) {
        return String.valueOf(navigationMapper.InsertNavigation(navigation_text));
    }

    /**
     * 根据id删除导航块
     * @param id
     * @return
     */
    @Override
    public String deleteDaoHang(int id) {
        return String.valueOf(navigationMapper.deleteDaoHang(id));
    }

    /**
     * 根据id更改导航块内容
     * @param navigation
     * @param id
     * @return
     */
    @Override
    public String updateDaoHang(String navigation, int id) {
        return String.valueOf(navigationMapper.updateDaoHang(id,navigation));
    }


}
