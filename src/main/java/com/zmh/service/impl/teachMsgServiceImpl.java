package com.zmh.service.impl;

import com.zmh.mapper.teachMsgMapper;
import com.zmh.pojo.teachMsg;
import com.zmh.service.teachMsgService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/*
 *@author zmh
 *@date 2022/5/10-9:34
 */
@Service
@Transactional(rollbackFor=Exception.class)
public class teachMsgServiceImpl implements teachMsgService {
    @Resource
    teachMsgMapper teachMsgMapper;
    @Override
    public List<teachMsg> findAll() {
        return teachMsgMapper.findAll();
    }

    @Override
    public List<teachMsg> findById(int id) {
        return teachMsgMapper.findById(id);
    }

    @Override
    public int updateMsgById(int id, String teachMsg, String teachHref, String teachDate) {
        return teachMsgMapper.updateMsgById(id,teachMsg,teachHref,teachDate);
    }

    @Override
    public int addMsg(String teachMsg, String teachHref, String teachDate) {
        return teachMsgMapper.addMsg(teachMsg,teachHref,teachDate);
    }

    @Override
    public int deleteMsg(int id) {
        return teachMsgMapper.deleteMsg(id);
    }

}
