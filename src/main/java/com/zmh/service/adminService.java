package com.zmh.service;

import com.zmh.pojo.adminteam;

import java.util.List;

/*
 *@author zmh
 *@date 2022/5/7-10:10
 */
public interface adminService {
    int findId(String adminName,String adminWord);
    List<adminteam> findMessage(String adminName);
    List<adminteam> findAdmin();
}
