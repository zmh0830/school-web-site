package com.zmh.service;

import com.zmh.pojo.navigationes;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/*
 *@author zmh
 *@date 2022/5/7-23:38
 */

public interface navigationService {
    List<navigationes> findNavigation();
    String InsertNavigation(String navigation_text);
    String deleteDaoHang(int id);
    String updateDaoHang(String navigation,int id);
}
