package com.zmh.service;

import com.zmh.pojo.teachMsg;

import java.util.List;

/*
 *@author zmh
 *@date 2022/5/10-9:34
 */
public interface teachMsgService {
    List<teachMsg> findAll();

    List<teachMsg> findById(int id);

    int updateMsgById(int id, String teachMsg, String teachHref, String teachDate);
    int addMsg(String teachMsg, String teachHref, String teachDate);
    int deleteMsg(int id);
}