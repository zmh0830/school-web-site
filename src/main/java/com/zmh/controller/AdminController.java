package com.zmh.controller;

import com.zmh.pojo.adminteam;
import com.zmh.service.adminService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/*
 *@author zmh
 *@date 2022/5/7-10:07
 */
@Controller
public class AdminController {
    @Resource
    adminService adminService;

    /**
     * 登录后台并将adminName存放到session中
     * @param adminteam
     * @param model
     * @param session
     * @return
     */
    @RequestMapping("/loginIn")
    public String loginIn(@Valid adminteam adminteam, Model model, HttpSession session, BindingResult bindingResult){
        //表单验证
        if (bindingResult.hasErrors()){
            return "login";
        }else {
            try {
                adminService.findId(adminteam.getAdminName(), adminteam.getAdminWord());
            }catch (Exception e){
                model.addAttribute("msg","登入失败!");
                return "login";
            }
        }
        session.setAttribute("adminName",adminteam.getAdminName());
        return "HouTaiIndex";
    }


}
