package com.zmh.controller;

import com.zmh.pojo.adminteam;
import com.zmh.pojo.navigationes;
import com.zmh.pojo.teachMsg;
import com.zmh.service.navigationService;
import com.zmh.service.teachMsgService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

/*
 *@author zmh
 *@date 2022/5/7-23:24
 */
@Controller
public class IndexController {
    @Resource
    navigationService navigationService;
    @Resource
    teachMsgService teachMsgService;
    /**
     * 更新页面
     * @param model
     * @return
     */
    @RequestMapping(value = {"/","/exit"})
    public String index(Model model, HttpSession session){
        List<navigationes> navigation = navigationService.findNavigation();
        List<teachMsg> teachMsg = teachMsgService.findAll();
        model.addAttribute("navigation",navigation);
        model.addAttribute("teachMsg",teachMsg);
        session.removeAttribute("adminName");
        return "index";
    }

}
