package com.zmh.controller;

import com.zmh.pojo.adminteam;
import com.zmh.pojo.navigationes;
import com.zmh.pojo.teachMsg;
import com.zmh.service.adminService;
import com.zmh.service.navigationService;
import com.zmh.service.teachMsgService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/*
 *@author zmh
 *@date 2022/5/7-11:48
 */
@Controller
public class HouTaiController {
    @Resource
    navigationService navigationService;
    @Resource
    adminService adminService;
    @Resource
    teachMsgService teachMsgService;

    /**
     * 查询导航栏内容并存放到对象域
     * @param model
     * @return
     */
    @RequestMapping("/DaoHangList")
    public String DaoHangList(Model model){
        try {
            List<navigationes> navigation = navigationService.findNavigation();
            model.addAttribute("navigation",navigation);
        }catch (Exception e) {
            model.addAttribute("msg","查询导航栏失败!");
            return "error";
        }
        return "DaoHangList";
    }

    /**
     * 插入导航栏内容并更新
     * @param model
     * @param navigationes
     * @return
     */
    @RequestMapping("/addNavigation")
    public String addNavigation(Model model, navigationes navigationes){
        try {
            navigationService.InsertNavigation(navigationes.getNavigation());
        }catch (Exception e){
            model.addAttribute("msg","添加失败");
            return "addDaoHang";
        }
        return "redirect:/DaoHangList";
    }

    /**
     * 根据id删除导航块
     * @param model
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("/deleteDaoHang")
    public String deleteDaoHang(Model model, HttpServletRequest request,int id){
        try {
            navigationService.deleteDaoHang(id);
        }catch (Exception e){
            model.addAttribute("msg","删除失败!");
            return "error";
        }
        return "redirect:/DaoHangList";
    }

    /**
     * 将导航块id内容放入对象域
     * @param id
     * @param navigation
     * @param model
     * @return
     */
    @RequestMapping("/updateDaoHang")
    public String updateDaoHang(int id,String navigation,Model model){
        model.addAttribute("id",id);
        model.addAttribute("navigation",navigation);
        return "updateDaoHang";
    }

    /**
     * 根据id更改导航块内容
     * @param id
     * @param model
     * @param request
     * @return
     */
    @RequestMapping("/updateDaoHang2")
    public String updateDaoHang2(int id,Model model,HttpServletRequest request){
        try {
            navigationService.updateDaoHang(request.getParameter("updateMsg"),id);
        }catch (Exception e){
            model.addAttribute("msg","编辑失败！");
            return  "updateDaoHang";
        }
        return "redirect:/DaoHangList";
    }

    /**
     * 根据sessionName查询展示基本资料
     * @param model
     * @param session
     * @return
     */
    @RequestMapping("/member_show")
    public String member_show(Model model, HttpSession session){
        Object adminName = session.getAttribute("adminName");
        List<adminteam> message = adminService.findMessage((String) adminName);
        model.addAttribute("messages",message);
        return "member_show";
    }

    /**
     * To系统设置页面
     * @return
     */
    @RequestMapping("/sys_set")
    public String sys_set(){
        return "sys_set";
    }

    /**
     * To屏蔽词页面
     * @return
     */
    @RequestMapping("/sys_shield")
    public String sys_shield(){
        return "sys_shield";
    }

    /**
     * To角色权限管理页面
     * @return
     */
    @RequestMapping("/role_edit")
    public String role_edit(){
        return "role_edit";
    }

    /**
     * To第三方登录页面
     * @return
     */
    @RequestMapping("/sys_log")
    public String sys_log(){
        return "sys_log";
    }

    /**
     * 查询管理员列表
     * @param model
     * @return
     */
    @RequestMapping("/adminList")
    public String adminList(Model model){
        List<adminteam> list = adminService.findAdmin();
        model.addAttribute("list",list);
        return "adminList";
    }

    @RequestMapping("/teachMsg")
    public String teachMsg(Model model){
        List<teachMsg> list = teachMsgService.findAll();
        model.addAttribute("list",list);
        return "teachMsg";
    }

    @RequestMapping("/updateTeachMsg")
    public String updateTeachMsg(Model model,int id){
        List<teachMsg> list = teachMsgService.findById(id);
        model.addAttribute("list",list);
        return "updateTeachMsg";
    }
    @RequestMapping("/updateTeachMsg2")
    public String updateTeachMsg2(Model model,teachMsg teachMsgPojo){
        int id = teachMsgPojo.getId();
        String teachMsg = teachMsgPojo.getTeachMsg();
        String teachHref = teachMsgPojo.getTeachHref();
        String teachDate = teachMsgPojo.getTeachDate();
        try {
            teachMsgService.updateMsgById(id,teachMsg,teachHref,teachDate);
        }catch (Exception e){
            model.addAttribute("msg","修改失败");
            return "error";
        }
        return "redirect:/teachMsg";
    }

    @RequestMapping("/addTeachMsg")
    public String addTeachMsg(){
        return "addTeachMsg";
    }

    @RequestMapping("/addTeachMsg2")
    public String addTeachMsg2(Model model,teachMsg teachMsgPojo){
        String teachMsg = teachMsgPojo.getTeachMsg();
        String teachHref = teachMsgPojo.getTeachHref();
        String teachDate = teachMsgPojo.getTeachDate();
        System.out.println(teachMsg);
        try {
            teachMsgService.addMsg(teachMsg,teachHref,teachDate);
        }catch (Exception e){
            model.addAttribute("msg","添加失败");
            return "error";
        }
        return "redirect:/teachMsg";
    }

    @RequestMapping("deleteTeachMsg")
    public String deleteTeachMsg(Model model,int id){
        try {
           teachMsgService.deleteMsg(id);
        }catch (Exception e){
            model.addAttribute("msg","删除失败");
            return "error";
        }
        return "redirect:/teachMsg";
    }
}
